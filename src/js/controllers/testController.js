/* import { updateLocale } from 'moment'; */
import { response } from '../app';
import ForecastService from '../util/forecastService';
import Plant from '../models/plant.js';
const { datastore } = require('../server');

const fetch = require('node-fetch');

const TestController = {};

// TEST FUNCTION
TestController.test = async (req, res) => {
  const plantId = req.params.plantId;
  const apiKey = req.header('Authorization');

  let testPlant = new Plant();

  // Get plant info
  let dataPlant = await fetch('https://api.epspot.com/v3/plant/' + plantId, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      Authorization: apiKey,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      return data;
    });

  //console.log(dataPlant.fuseGroups);
  //fuseload = await fuseLoad.json();
  try {
    await testPlant.v2Data(apiKey, plantId);
    //await testPlant.fuseGroup.Populate(dataPlant.fuseGroups, apiKey);
    await testPlant.loadData(apiKey, plantId, res);
  } catch (error) {
    console.log('error', error);
    res.send('error', error);
  }

  console.log(testPlant);
  res.send(testPlant);
};

TestController.test2 = async (req, res) => {
  let hung = {
    id: 123123,
    name: 'gjjh',
    description: 'ggjgh',
    address: [
      {
        1: 'asdasd',
        2: 'asdassfgs',
      },
    ],
  };
  console.log('normal', hung);
  hung = JSON.stringify(hung);
  hungjson = JSON.parse(hung);
};
// Test function 2
TestController.testPlant = async (req, res) => {};

/* Test function 3
TestController.testLimit = async (req, res) => {
  let apiKey = req.header('Authorization');
  let plantId = req.params.plantId;
  let values = {
    amount: req.body.amount,
  };

  // Returns 'Fuse Groups'
  let dataPlant = await fetch('https://api.epspot.com/v3/plant/' + plantId, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      Authorization: apiKey,
    },
  });

  let fuseGroup = new FuseGroup();
  
  dataPlant = await dataPlant.json();
  let fuseGroups = dataPlant.fuseGroups;
  let totalAmpere = await fuseGroups.reduce(function (a, b) {
    return {
      ampere: a.ampere + b.ampere,
    };
  });

  // console.log('apiKey', apiKey);
  // console.log('plantId', plantId);
  // console.log('values.amount', values.amount);
  // console.log('dataPlant', dataPlant);
  console.log('fuseGroups', fuseGroups);
  // console.log('totalAmpere', totalAmpere);

  let responseMessage = await Promise.all(
    fuseGroups.map(async (fuses) => {
      let fuse = await fetch(
        'https://api.epspot.com/v3/fusegroup/momentary_limit/' +
          fuses.id +
          '/' +
          Math.round((fuses.ampere * values.amount) / 100),
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            Authorization: apiKey,
          },
        }
      );
      return fuse.json();
    })
  );

  res.send(responseMessage);
};*/

//res.send(responseMessage);

//};

export default TestController;
