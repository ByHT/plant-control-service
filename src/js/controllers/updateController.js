/* import { updateLocale } from 'moment'; */
import { response } from '../app';
import ForecastService from '../util/forecastService';
import Plant from '../models/plant.js';
const { datastore } = require('../server');
const fetch = require('node-fetch');

const UpdateController = {};

// Set forecast data with POST
UpdateController.throughputLimit = async function (req, res) {
  try {
    let plant = new Plant();
    await plant.loadData(req.header('Authorization'), req.params.plantId, res);
    await plant.v2Data(req.header('Authorization'), req.params.plantId);

    let totalReduction = req.body.value;

    // Set all fuses momentary limit to their max value
    if (totalReduction == 0) {
      plant.fuseGroup.forEach((fuse) => {
        console.log('Set', fuse.name, ': ', fuse.ampere);
      });
    }
    // Set fuses momentary limit in proportion to potential
    else {
      const totalThroughputReading = [
        plant.throughputReading.p1,
        plant.throughputReading.p2,
        plant.throughputReading.p3,
      ];
      const totalMinThroughput =
        plant.minThroughput.p1 +
        plant.minThroughput.p2 +
        plant.minThroughput.p3;
      const totalPotential =
        totalThroughputReading.reduce((pv, cv) => pv + cv, 0) -
        totalMinThroughput;

      if (totalReduction > totalPotential) {
        totalReduction = totalPotential;
      }

      console.log(
        'totalThroughputReading',
        totalThroughputReading.reduce((pv, cv) => pv + cv, 0)
      );
      console.log('totalThroughputReading', totalThroughputReading);
      console.log('totalMinThroughput', totalMinThroughput);
      console.log('totalPotential', totalPotential);
      console.log('totalReduction', totalReduction);

      plant.fuseGroup.forEach((fuse) => {
        setLimit(fuse, totalPotential, totalReduction);
      });

      res.send(plant);
    }
  } catch (error) {
    console.log('ERROR: ', error);
    res.send(error);
  }
};

async function setLimit(inputFuse, potential, reduction) {
  const minAmps = 1000;

  let fuse = {
    name: inputFuse.name,
    id: inputFuse.id,
    momentaryLimit: {
      l1: inputFuse.momentaryLimit.L1,
      l2: inputFuse.momentaryLimit.L2,
      l3: inputFuse.momentaryLimit.L3,
    },
    throughputReading: {
      l1: Math.round(inputFuse.load.L1 / 100) / 10,
      l2: Math.round(inputFuse.load.L2 / 100) / 10,
      l3: Math.round(inputFuse.load.L3 / 100) / 10,
    },
    sessions: {
      l1: inputFuse.sessions.filter(
        (element) => element.fuseLoadData.L1 > minAmps
      ).length,
      l2: inputFuse.sessions.filter(
        (element) => element.fuseLoadData.L2 > minAmps
      ).length,
      l3: inputFuse.sessions.filter(
        (element) => element.fuseLoadData.L3 > minAmps
      ).length,
    },
    potential: {
      l1: 0,
      l2: 0,
      l3: 0,
    },
    reduction: {
      l1: 0,
      l2: 0,
      l3: 0,
    },
    momentaryLimit: {
      l1: 0,
      l2: 0,
      l3: 0,
    },
  };

  fuse.potential.l1 = fuse.throughputReading.l1 - fuse.sessions.l1 * 8;
  fuse.potential.l2 = fuse.throughputReading.l2 - fuse.sessions.l2 * 8;
  fuse.potential.l3 = fuse.throughputReading.l3 - fuse.sessions.l3 * 8;

  if (reduction == 0) {
    fuse.momentaryLimit.l1 = inputFuse.ampere;
    fuse.momentaryLimit.l2 = inputFuse.ampere;
    fuse.momentaryLimit.l3 = inputFuse.ampere;
  } else {
    fuse.reduction.l1 = (fuse.potential.l1 / potential) * reduction;
    fuse.reduction.l2 = (fuse.potential.l2 / potential) * reduction;
    fuse.reduction.l3 = (fuse.potential.l3 / potential) * reduction;
    fuse.momentaryLimit.l1 = inputFuse.momentaryLimit.L1 - fuse.reduction.l1;
    fuse.momentaryLimit.l2 = inputFuse.momentaryLimit.L2 - fuse.reduction.l2;
    fuse.momentaryLimit.l3 = inputFuse.momentaryLimit.L3 - fuse.reduction.l3;
  }

  console.log(fuse);
}

export default UpdateController;
