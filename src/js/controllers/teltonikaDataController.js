import { response } from '../app';
import ForecastService from '../util/forecastService';
import Plant from '../models/plant.js';
const { datastore } = require('../server');
const fetch = require('node-fetch');

const TeltonikaDataController = {};

TeltonikaDataController.test = async (req, res) => {
  try {
    let reqBody = req.body;

    // reqBody[0].data = reqBody[0].data.replace(/[\[\]']| +/g, '');
    // reqBody[0].data = reqBody[0].data.replace(/,([^,]*)$/, '$1');
    // reqBody[0].data = reqBody[0].data.split(',').map(Number);

    let throughputReading = {
      p1: reqBody.data[0],
      p2: reqBody.data[1],
      p3: reqBody.data[2],
    };
    const keyApi = datastore.key(['ApiKey', req.header('Authorization')]);
    const [auth] = await datastore.get(keyApi);

    if (auth === undefined) {
      throw 'BAD API KEY';
    }

    //  V2Provider info with filter
    const v2ProviderQuery = datastore.createQuery('V2Provider');
    v2ProviderQuery.filter('legacyId', auth.providerId);
    const v2Provider = await v2ProviderQuery.run().then((data) => {
      return data;
    });

    if (v2Provider[0][0][datastore.KEY].name === undefined) {
      let errStr = 'NO PROVIDER WITH API KEY:' + auth.providerId;
      throw errStr;
    } else if (reqBody.plantId === undefined) {
      let errStr = 'NO PLANT ID FROM PROVIDER: ' + auth.providerId;
      throw errStr;
    }

    // Get V2Plant info
    const keyPlant = await datastore.key([
      'V2Provider',
      v2Provider[0][0][datastore.KEY].name,
      'V2Plant',
      reqBody.plantId,
    ]);

    const [v2Plant] = await datastore.get(keyPlant);

    //updateEntity(keyPlant, throughputReading);

    if (reqBody.startRegister == 13) {
      console.log(v2Plant.name, 'Ampere*1000 value: ', reqBody.data);
      res.status(200).send('OK');
    } else if (reqBody.startRegister == 19) {
      console.log(v2Plant.name, 'Watt*10 value: ', reqBody.data);
      res.status(200).send('OK');
    }
  } catch (error) {
    console.log('ERROR: /plantConsumedPower', error);
    console.log('API Key: ', req.header('Authorization'));
    console.log('Request body: ', req.body);
    res.status(503).send('Bad input');
  }
};

// Not in use
async function updateEntity(key, newValue) {
  const transaction = datastore.transaction();

  transaction
    .run()
    .then(() => transaction.get(key))
    .then((result) => {
      const entity = result[0];
      entity.throughput.p1 = newValue.p1;
      entity.throughput.p2 = newValue.p2;
      entity.throughput.p3 = newValue.p3;
      console.log(entity.name + '.throughput updated: ', entity.throughput);

      transaction.save({
        key: key,
        data: entity,
      });

      return transaction.commit();
    })
    .catch((err) => {
      console.log('ERROR: /plantConsumedPower BAD PLANT ID', err);
      transaction.rollback();
    });
}
export default TeltonikaDataController;
