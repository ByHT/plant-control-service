/* import { updateLocale } from 'moment'; */
import { response } from '../app';
import ForecastService from '../util/forecastService';
import Plant from '../models/plant.js';
const { datastore } = require('../server');
const fetch = require('node-fetch');

const PlantsController = {};

PlantsController.listPlants = async function (req, res) {
  const apiKey = req.header('Authorization');

  try {
    fetch('https://api.epspot.com/v3/plants/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: apiKey,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        res.send(data);
      });
  } catch (error) {
    res.send(error);
  }
};
export default PlantsController;
