const express = require('express');

/* process.env.GOOGLE_APPLICATION_CREDENTIALS = './keys/epspot-playground-271e99ea78e9.json'; */
//process.env.GOOGLE_APPLICATION_CREDENTIALS = './keys/epspot-production-d90f80058a59.json'

const app = express();

app.get('/', (req, res) => {
  res.render('siteRoot', {
    layout: '',
  });
});

app.get('/favicon.ico', (req, res) => res.status(204).end());

module.exports = app;
