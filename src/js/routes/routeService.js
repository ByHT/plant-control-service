/* import ModelService from '../models/modelService'; */

const RouteService = {};

RouteService.handleError = function (res, error) {
  console.log(error);
  if (error && error.code && error.epspotmessage) {
    // our own error message
    res
      .status(error.code)
      .send({ code: error.epspotcode, message: error.epspotmessage });
  } else if (error && error.code) {
    if (error.code === 'ERR_VALIDATION') {
      res
        .status(400)
        .send({ code: error.code, message: error.errors[0].message });
    } else {
      res.status(500).send({ code: error.code, message: 'Internal Error' });
    }
  } else {
    res.status(500).send({ code: 0, message: 'Internal Error' });
  }
};

export default RouteService;
