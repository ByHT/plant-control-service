import ForecastService from "../js/util/forecastService";
import { expect } from "chai";

describe("ForecastService", () => {
  describe("someUsefulFeature()", () => {
    context("testing the useful feature", () => {
      it("should return an uppercase value", () => {
        const res = ForecastService.someUsefulFeature('kalle');
        expect(res === 'KALLE');
      });
    });
  });
});
