const { datastore } = require('../server');
const fetch = require('node-fetch');
//Create Plant object
function Plant() {
  (this.id = null),
    (this.name = null),
    (this.description = null),
    (this.address = null),
    (this.gridConnectionId = null),
    (this.gridOwner = null),
    (this.gridRegion = null),
    (this.masterFuse = null),
    (this.maxThroughput = {
      p1: null,
      p2: null,
      p3: null,
    }),
    (this.minThroughput = {
      p1: 0,
      p2: 0,
      p3: 0,
    }),
    (this.throughputLimit = {
      p1: null,
      p2: null,
      p3: null,
    }),
    (this.throughputReading = {
      p1: null,
      p2: null,
      p3: null,
    }),
    (this.throughputLimitActivated = null),
    (this.createdOn = null),
    (this.fuseGroup = []);
}
//Returns data from Datastore
Plant.prototype.v2Data = async function (apiKey, plantId) {
  const keyApi = datastore.key(['ApiKey', apiKey]);
  const [auth] = await datastore.get(keyApi);
  // Get V2Provider info with filter
  const v2ProviderQuery = datastore.createQuery('V2Provider');
  v2ProviderQuery.filter('legacyId', auth.providerId);
  const v2Provider = await v2ProviderQuery.run().then((data) => {
    return data;
  });
  // Get V2Plant info
  const keyPlant = datastore.key([
    'V2Provider',
    v2Provider[0][0][datastore.KEY].name,
    'V2Plant',
    plantId,
  ]);
  const v2Plant = await datastore.get(keyPlant);
  //Populates Plant object with data from Datastore
  this.id = validate(v2Plant[0][datastore.KEY].name);
  this.name = validate(v2Plant[0].name);
  this.description = validate(v2Plant[0].description);
  this.address = validate(v2Plant[0].address);
  this.gridConnectionId = validate(v2Plant[0].gridConnectionId);
  this.gridOwner = validate(v2Plant[0].gridOwner);
  this.gridRegion = validate(v2Plant[0].gridRegion);
  this.masterFuse = validate(v2Plant[0].masterFuse);
  this.maxThroughput = validate(v2Plant[0].maxThroughput);
  //this.minThroughput = validate(v2Plant[0].minThroughput);
  this.throughputLimit = validate(v2Plant[0].throughputLimit);
  this.throughputLimitActivated = validate(v2Plant[0].throughputLimitActivated);
  this.createdOn = validate(v2Plant[0].createdOn);
};
// Return the fuse load
Plant.prototype.loadData = async function (apiKey, plantId, res) {
  let dataPlant = await fetch('https://api.epspot.com/v3/plant/' + plantId, {
    method: 'get',
    headers: {
      Accept: 'application/json',
      Authorization: apiKey,
    },
  });
  res.status(dataPlant.status);
  let dataPlantJson = await dataPlant.json();
  // If request returns error code, send
  if (dataPlant.status < 200 || dataPlant.status > 299) {
    res.send(dataPlantJson);
  } else {
    // GET active load from all fuses
    await Promise.all(
      dataPlantJson.fuseGroups.map((fuses) =>
        fetch('https://api.epspot.com/v3/fusegroup/load/' + fuses.id, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            Authorization: apiKey,
          },
        })
          // Add load values to response message for each element
          .then((response) => response.json())
          .then((dataFuse) => {
            this.throughputReading.p1 += Math.round(dataFuse.load.L1 / 1000);
            this.throughputReading.p2 += Math.round(dataFuse.load.L2 / 1000);
            this.throughputReading.p3 += Math.round(dataFuse.load.L3 / 1000);
            // Remove sessions without load
            // And set Minimum Throughput
            let minAmps = 1000;
            for (var i = 0; i < dataFuse.sessions.length; i++) {
              if (
                dataFuse.sessions[i].fuseLoadData.L1 +
                  dataFuse.sessions[i].fuseLoadData.L2 +
                  dataFuse.sessions[i].fuseLoadData.L3 >
                  minAmps &&
                dataFuse.sessions[i].evse == true
              ) {
                if (dataFuse.sessions[i].fuseLoadData.L1 > minAmps) {
                  this.minThroughput.p1 += 8;
                }
                if (dataFuse.sessions[i].fuseLoadData.L2 > minAmps) {
                  this.minThroughput.p2 += 8;
                }
                if (dataFuse.sessions[i].fuseLoadData.L3 > minAmps) {
                  this.minThroughput.p3 += 8;
                }
              } else {
                dataFuse.sessions.splice(i, 1);
                i--;
              }
            }
            this.fuseGroup.push(dataFuse);
            return dataFuse;
          })
      )
    );
  }
};
function validate(input) {
  if (input != undefined) {
    return input;
  } else {
    return null;
  }
}
export default Plant;
