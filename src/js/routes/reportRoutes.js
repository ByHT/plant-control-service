import PlantsController from '../controllers/plantsController';
import TestController from '../controllers/testController';
import ServicePlantController from '../controllers/servicePlantController';
import TeltonikaOnOffController from '../controllers/teltonikaOnOffController';
import TeltonikaDataController from '../controllers/teltonikaDataController';
import UpdateController from '../controllers/updateController';

const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.send('EpSpot Report API TEST');
});

// HTTP request method list
router.post('/plant/:plantId/update', UpdateController.throughputLimit);
router.post('/plantConsumedPower', TeltonikaDataController.test);
router.post('/plantLimitActivate', TeltonikaOnOffController.test);
router.get('/plant/:plantId', ServicePlantController.throughputReading);
router.get('/plants', PlantsController.listPlants);

router.get('/test/:plantId', TestController.test);
router.get('/test2', TestController.test2);
// router.post('/testPlant/:plantId/update', TestController.testLimit);

router.get('/servicePlant/:plantId', ServicePlantController.getServicePlant);
router.post('/servicePlant/:plantId', ServicePlantController.updateServicePlant);

module.exports = router;
