const { gstore } = require('../server');
const { Schema } = gstore;

const ServicePlant = new Schema({
  createdOn: { type: Date, default: gstore.defaultValues.NOW, read: true, write: false },
  plantId: { type: String, required: true },
  serviceFuse: { type: Number },
  gridConnectionId: { type: String },
  consumedPowerSource: { type: String, optional: true },
  producedPower: { type: Object, optional: true, validate: { rule: validatePhases } },
  reservedPower: { type: Object, optional: true, validate: { rule: validatePhases } },
  epspotLimitationActivated: { type: Boolean },
  epspotLimitation: { type: Object, optional: true, validate: { rule: validateLimitation } },
  throughputReading: { type: Object, write: false, read: true },
});

function validatePhases(obj) {
  // Valid Keys
  const allowedProps = ['p1', 'p2', 'p3'];
  let keys = Object.keys(obj);
  const onlyHasAllowedProps = keys.every((item) => allowedProps.includes(item));

  // Valid values values
  let values = Object.values(obj);
  let onlyHasAllowedValues = values.every((item) => typeof item === 'number');

  if (!onlyHasAllowedProps || !onlyHasAllowedValues) return false;
  return true;
}

function validateLimitation(limitation, validator) {
  // limitation
  const allowedProps = ['mode', 'value', 'validUntil'];
  let keys = Object.keys(limitation);
  const onlyHasAllowedProps = keys.every((item) => allowedProps.includes(item));

  if (!onlyHasAllowedProps) return false;
  // mode
  const allowedModeValues = ['BALANCED', 'RELATIVE', 'ABSOLUTE'];
  const modeValue = limitation.mode;

  const allowedModeValue = allowedModeValues.includes(modeValue);

  if (!allowedModeValue) return false;

  //value
  if (modeValue == 'BALANCED' && limitation.value && limitation.value > 0) return false;

  // validUntil

  if (limitation.validUntil) {
    const validDate =
      validator.isDate(limitation.validUntil) && validator.isAfter(limitation.validUntil);
    console.log(validDate);
    if (!validDate) return false;
  }
  return true;
}

module.exports = gstore.model('ServicePlant', ServicePlant);
