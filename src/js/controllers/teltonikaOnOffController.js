/* import { updateLocale } from 'moment';
import { response } from '../app';
import ForecastService from '../util/forecastService';
import Plant from '../models/plant.js';
 const fetch = require('node-fetch'); */
const { datastore } = require('../server');

const TeltonikaOnOffController = {};

TeltonikaOnOffController.test = async (req, res) => {
  console.log('Teltonika On/Off', req.body);

  const keyApi = datastore.key(['ApiKey', req.header('Authorization')]);
  const [auth] = await datastore.get(keyApi);

  // Get V2Provider info with filter
  const v2ProviderQuery = datastore.createQuery('V2Provider');
  v2ProviderQuery.filter('legacyId', auth.providerId);
  const v2Provider = await v2ProviderQuery.run().then((data) => {
    return data;
  });

  // Get V2Plant info
  const keyPlant = await datastore.key([
    'V2Provider',
    v2Provider[0][0][datastore.KEY].name,
    'V2Plant',
    req.body.plantId,
  ]);

  if (parseFloat(req.body.analogInput) > 1) {
    updateEntity(keyPlant, true);
  } else {
    updateEntity(keyPlant, false);
  }

  res.send('OK');
};

async function updateEntity(key, newValue) {
  const transaction = datastore.transaction();

  transaction
    .run()
    .then(() => transaction.get(key))
    .then((result) => {
      const entity = result[0];
      entity.throughputLimitActivated = newValue;
      console.log(entity.name + ' throughputLimitActivated' + ' changed to: ' + newValue);

      transaction.save({
        key: key,
        data: entity,
      });

      return transaction.commit();
    })
    .catch(() => transaction.rollback());
}

export default TeltonikaOnOffController;
