const PHYSICAL_HIERARCHY = [
  'V2Provider',
  'V2Plant',
  'V2Location',
  'V2Fuse',
  'V2Outlet',
];
const PHYSICAL_HIERARCHY_QUERY_PARAMS = [
  'providerId',
  'plantId',
  'locationId',
  'fuseId',
  'outletId',
];

const MEMBER_HIERARCHY = ['V2Provider', 'Member'];
const MEMBER_HIERARCHY_QUERY_PARAMS = ['providerId', 'memberId'];

const USER_HIERARCHY = ['User', 'PaymentReceipt'];
const USER_HIERARCHY_QUERY_PARAMS = ['userId', 'paymentReceiptId'];

const ModelService = {};

/*  Takes a gstore-node entity and converts it to an object corresponding to the Epspot model.
 *   Conversion is basically to remove the internal __key object (after exposing relevant data from it)
 *   and remove all props which the requesting role doesn't have the right to view.
 */
ModelService.processModel = function (req, model) {
  let jsonModel = null;
  if (model.entityData) {
    // if model is an entity
    jsonModel = model.plain({ showKey: true });
    //console.log(jsonModel);
  } else {
    // model is already plain data
    jsonModel = model;
  }

  if (jsonModel.hasOwnProperty('__key')) {
    jsonModel.kind = jsonModel.__key.kind;
    if (
      req.query.includeParents &&
      jsonModel.__key.path &&
      jsonModel.__key.path.length >= 2
    ) {
      jsonModel.parents = jsonModel.__key.path.slice(
        0,
        jsonModel.__key.path.length - 2
      );
    }
    delete jsonModel.__key;
  }
  let res = RoleService.removeUnathorizedProps(req, jsonModel);
  //console.log(res);
  return res;
};

export default ModelService;
