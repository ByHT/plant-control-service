# Reporting Service

Rest API for EpSpot reports and statistics... 
This is a sceleton project, just including a single endpoint for http POST:
```
POST https://reporting-service-dot-epspot-playground.appspot.com/reports/forecastData
```

It's pretty much just a hello-world example to base further development on.


## Installation


```bash
npm install 
```

## Development

### Google Cloud Platform Service Account
You will need a key to access gcloud services.

To get a private key, go to [Google Cloud Platform Console](https://console.cloud.google.com/iam-admin/serviceaccounts/) and make sure you´re logged in with your EpSpot account. Select the project you´ve been invited to.

Click on the service account with name "app-engine-dev"

Under the Key section, press ADD KEY - Generate new key - choose JSON as key type, a JSON file should have been downloaded.

Follow these steps to set a local variable (macOS) to make references to the GCP key files. These references are needed for the npm scripts to work. (GCPPRODFILEPATH is the key variable for the producion project, GCPPGFILEPATH is the key variable for the playground project):
  1. To open bash profile, use command ```vim ~/./bash_profile``` in terminal.
  2. To set and export the variable enter ```export GCPPGFILEPATH=<path-to-file>``` and ```export GCPPRODFILEPATH=<path-to-file>``` replace    <path-to-file> with the path to the downloaded files in previous steps.
  3. Use ```source ~/./bash_profile``` or restart terminal to load the new changes
  4. To see that everything works ```echo $GCPPGFILEPATH```
  5. Now in package.json you can use the new variables in the same way you did in terminal, example: : ```export GOOGLE_APPLICATION_CREDENTIALS=\"${GCPPGFILEPATH}\"```
   
Note: Do this to all the projects you have access to.



To start the local DB emulator (needs to be done in a separate shell)

```bash
npm run start-local-db
```
When the local DB emulator is running you can start the server locally with
```bash
npm run local-db-dev
```

To use the local server towards the playground DB 
(this is the easiest way for local development...)
```bash
npm run playground-db-dev
```

To use the local server towards the production DB 
```bash
npm run production-db-dev
```

## Deployment

Deployment to Production environment
```bash
npm run deploy
```

Deployment to Playground environment
```bash
npm run deploy-playground
```
## Tests

### Run all tests
```bash
npm run test
```

### Run single tests
Change the test script in package.json. Add testfile to the pathname to "./src/test/" eg ./src/test/file.test.js and then run:
```bash
npm run test
```
