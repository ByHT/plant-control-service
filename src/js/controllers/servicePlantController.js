const PlantControl = require('../models/plantControl');
import RouteService from '../routes/routeService';
import Plant from '../models/plant';

const ServicePlantController = {};

// Get thoroughput reading on specific plant
ServicePlantController.throughputReading = async function (req, res) {
  let plant = new Plant();
  try {
    //Get Load data from FuseLoad API
    await plant.loadData(req.header('Authorization'), req.params.plantId, res);
    //Get data from Datastore
    await plant.v2Data(req.header('Authorization'), req.params.plantId);
    plant.fuseGroup.forEach((fuse) => {
      fuse.sessions = undefined;
    });
    console.log(plant);
    res.send(plant);
  } catch (error) {
    console.log('ERROR: /plant/:plantId', error);
    res.send(error);
  }
};

//post Plant settings create
/* PlantController.create = async (req, res) => {
  const plantId = req.params.plantId;
  const newReportSchema = new PlantControl({
    plantId: plantId, // unique plant ID
    producedPower: {
      mode: 'DISABLED',
    },
    consumedPower: {
      mode: 'DISABLED',
    },
    powerReserve: {
      mode: 'DISABLED',
    },
    momentaryPowerReduction: {
      mode: 'DISABLED',
    },
  });

  try {
    console.log('newReportSchema', newReportSchema);
    //see if plant got a report setting
    const existingEntity = await PlantControl.findOne({
      plantId: req.params.plantId,
    });
    if (existingEntity) throw res.status(400).send('ERROR_ALREADY_EXISTS');
    //otherwise create new report with default disabled mode
    const newEntity = await newReportSchema
      .save()
      .then((newEntity) => newEntity.plain());
    console.log('newEntity ********************', newEntity);
    res.status(200).send(newEntity);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};
 

PlantController.delete = async (req, res) => {
  try {
    //see if plant got a report setting
    const existingEntity = await PlantControl.findOne({
      plantId: req.params.plantId,
    }).then((existingEntity) => existingEntity.plain());
    if (!existingEntity) throw res.status(400).send('ERROR_NOT_EXISTS');
    //otherwise delete report with default disabled mode
    await PlantControl.delete(Number(existingEntity.id));
    res.status(204).send();
  } catch (error) {
    res.send(error);
  }
};
*/
const getCurrentPlantControl = async (plantId) => {
  const currentPlantControl = await PlantControl.findOne({
    plantId: plantId,
  });
  if (currentPlantControl) return currentPlantControl.plain();

  return null;
};

const updateCurrentPlantControl = async (plantId, updateData) => {
  try {
    const currentEntity = await PlantControl.findOne({
      plantId: plantId,
    }).then((entity) => entity.plain());

    const updatedEntity = await PlantControl.update(
      Number(currentEntity.id),
      updateData
    ).then((entity) => entity.plain());
    return updatedEntity;
  } catch (error) {
    throw {
      code: 500,
      message: `There was an error updating entity plant with id ${plantId}, ${error}`,
    };
  }
};

const createNewPlantControl = async (plantId) => {
  const newPlantControl = new PlantControl({
    plantId: plantId, // unique plant ID
  });

  try {
    const newEntity = await newPlantControl.save().then((newEntity) => newEntity.plain());

    return newEntity;
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};
//get Plant Produced Power settings
ServicePlantController.getServicePlant = async (req, res) => {
  const plantId = req.params.plantId;
  let result;
  try {
    result = await getCurrentPlantControl(plantId);
    if (!result) result = await createNewPlantControl(plantId);
    if (!result) throw { code: 500, error: 'internal server error' };

    res.status(200).send(result);
  } catch (error) {
    console.log(error);
    RouteService.handleError(res, error);
  }
};

//post Plant Produced Power settings
ServicePlantController.updateServicePlant = async function (req, res) {
  const plantId = req.params.plantId;
  let result;
  try {
    let currentPlantControl = await getCurrentPlantControl(plantId);
    if (!currentPlantControl) await createNewPlantControl(plantId);
    result = await updateCurrentPlantControl(plantId, req.body);
    res.status(200).send(result);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};

export default ServicePlantController;
