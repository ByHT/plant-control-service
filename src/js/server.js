const cors = require('cors');
const express = require('express');

const { Datastore } = require('@google-cloud/datastore');
const app = require('./app');

const datastore = new Datastore();
exports.datastore = datastore;

const { Gstore } = require('gstore-node');
const gstore = new Gstore({ errorOnEntityNotFound: false });
gstore.connect(datastore);
exports.gstore = gstore;

var reportsRouter = require('./routes/reportRoutes');

console.log('***** STARTING REPORTING SERVICE ****');

app.enable('trust proxy');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(reportsRouter);

// Listen to the App Engine-specified port, or 8080 otherwise
const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});

export default app;
